#!/usr/bin/python

"""
import of the needed libraries
"""
import sys
import redpitaya_scpi as scpi
import tkinter as Tk
import matplotlib.pyplot as plot

import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure

import time

# static IP address of the redpitaya
IP=sys.argv[1]

# parameters using to generate the force clamp signal
V1 = 1.0
V3 = 0.2
V5 = 0.4
V6 = 0.8
S1 = 10
S2 = 10
S3 = 10
S4 = 10
S5 = 10
S6 = 10
freq =8000
ampl = 1
# possible values : 1, 8, 64, 1024, 8192, 65536
decimation = 1
# buffer is readed from -8192 to 8192
delay = 8192
# 125 Msps 125*1048576
plotsamples = 131072000/(decimation*freq)

def generateSendSignal(bufferSize):
	"""
	static function to generate all the points of the signal

	Calculate each point of a force clamp signal, by calculating 
	the equation of each part of the signal. Then all the result 
	are scale for the buffer size.

	Parameters
	----------
	bufferSize : int
	    Size of the Redpitaya output buffer

	Returns
	-------
	list
	    All the points of the signal in a list

	"""
	# duration of the period
	Stot = S1+S2+S3+S4+S5+S6

	# scale the period to the buffer size
	i1 = S1*bufferSize/Stot 
	i2 = (S1+S2)*bufferSize/Stot 
	i3 = (S1+S2+S3)*bufferSize/Stot 
	i4 = (S1+S2+S3+S4)*bufferSize/Stot 
	i5 = (S1+S2+S3+S4+S5)*bufferSize/Stot 

	# calculate the equation of the three slopes
	a2 = (V3 - V1) / (i2 - i1)
	b2 = V1 - a2*i1
	a4 = (V5 - V3) / (i4 - i3) 
	b4 = V3 - a4*i3
	a5 = (V6 - V5) / (i5 - i4)
	b5 = V5 - a5*i4
	
	# set the first point to 0V because this value will set the default value of the port
	x = ''
	#x = '0, '
	# calculate all the point using the previous equations
	for i in range(1, bufferSize-1):
		if(i < i1):
			x += str(V1) + ', '
		elif(i < i2):
			x += str(a2*i+b2) + ', '
		elif(i < i3):
			x += str(V3) + ', '
		elif(i < i4):
			x += str(a4*i+b4) + ', '
		elif(i < i5):
			x += str(a5*i+b5) + ', '
		else:
			if(i != bufferSize-2): 
				x += str(V6) + ', '
			else:
				x += str(V6)
	# return all the points
	return x

class Application(Tk.Frame):
	"""
	class of the application
	
	extends Tk.Frame object
	
	Manage all the graphical part of the application

	"""
	def __init__(self, master=None):
		"""
		Basic initialize

		init the graphic part of the application and the connexion with the redpitaya.

		Parameters
		----------
		master : Tk
		    Tkinker main window object

		Returns
		-------

		"""
		# init the frame in the main window
		Tk.Frame.__init__(self, master)
		# init the graphical interface
		self.initUI(master)
		# init the connexion to the redpitaya
		self.initSignalConnexion()

	def initSignalConnexion(self):
		"""
		Function to init the connexion to the redpitaya

		init the connexion to the redpitaya and set all the parameters 
		to the source channel and the two input channels

		Parameters
		----------

		Returns
		-------

		"""
		# create the connexion
		self.rp_s = scpi.scpi(IP)
		# ask the buffer size
		self.rp_s.tx_txt('ACQ:BUF:SIZE?')
		bufferSize = int(self.rp_s.rx_txt())

		# generate the waveform according to the buffer size
		x = generateSendSignal(bufferSize)

		# reset old parameters of the input and output channels
		self.rp_s.tx_txt('GEN:RST')
		self.rp_s.tx_txt('ACQ:RST')

		# configure the output signal, arbitrary waveform and give all the points
		# set the frequecy and the amplitude
		self.rp_s.tx_txt('SOUR1:FUNC ARBITRARY')
		self.rp_s.tx_txt('SOUR1:TRAC:DATA:DATA ' + x)
		self.rp_s.tx_txt('SOUR1:FREQ:FIX ' + str(freq))
		self.rp_s.tx_txt('SOUR1:VOLT ' + str(ampl))

		# set channel 1 of the redpitaya on burst mode (pulse not continious) 
		self.rp_s.tx_txt('SOUR1:BURS:STAT ON')
		# and ask for one period by burst
		self.rp_s.tx_txt('SOUR1:BURS:NCYC 1')
		# enable the fast analog output
		self.rp_s.tx_txt('OUTPUT1:STATE ON')

		# set the decimation to the finest precision -> 1 
		# possible values : 1, 8, 64, 1024, 8192, 65536
		self.rp_s.tx_txt('ACQ:DEC ' + str(decimation))

		# set the trigger level at 0mV
		self.rp_s.tx_txt('ACQ:TRIG:LEV 0')
		# set the trigger delay in samples at 0ns
		self.rp_s.tx_txt('ACQ:TRIG:DLY ' + str(delay))

	def initUI(self, root):
		"""
		Function to init the graphical part

		init the two graphs to visualise the acquision of the two
		input channel on the redpitaya. init a "acquire" button which
		trigger an acquision by clicking on it.

		Parameters
		----------
		root : Tk window

		Returns
		-------

		"""
		# init a Tk figure with to subplot for the two visualisations
		f = Figure(figsize=(5, 4), dpi=100)
		self.a1 = f.add_subplot(211)
		self.a2 = f.add_subplot(212)
		
		# drawing of the graph and positioning it in the window
		canvas = FigureCanvasTkAgg(f, master=root)
		canvas.show()
		canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

		# add the matplotlib toolbar and positioning it
		toolbar = NavigationToolbar2TkAgg(canvas, root)
		toolbar.update()
		canvas._tkcanvas.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

		# add the button and set the callback function and positioning it
		button = Tk.Button(master=root, text='Acquerir', command=lambda: self.sendDisplaySignal(canvas, f))
		button.pack(side=Tk.BOTTOM)
		

	def sendDisplaySignal(self, canvas, f):
		"""
		Function to make and display an acquision

		Launch the trigger to send a signal and read the two inputs

		Parameters
		----------
		canvas : FigureCanasTkAgg
			to upload the graphs
		f : Figure
			to upload the graphs

		Returns
		-------

		"""
		# start the acquisition
		self.rp_s.tx_txt('ACQ:START')
		# set the trigger to a positif edge on a arbitrary waveform generator 
		self.rp_s.tx_txt('ACQ:TRIG AWG_PE')
		# trigger source 1 immediately
		self.rp_s.tx_txt('SOUR1:TRIG:IMM')

		# while the trigger not ok, wait
		while 1:
			self.rp_s.tx_txt('ACQ:TRIG:STAT?')
			if self.rp_s.rx_txt() == 'TD':
				break
		
		buff = []
		buff2 = []
		# get data from the first input channel
		self.rp_s.tx_txt('ACQ:SOUR1:DATA?')
		buff_string = self.rp_s.rx_txt()

		# get data from the second input channel
		self.rp_s.tx_txt('ACQ:SOUR2:DATA?')
		buff_string2 = self.rp_s.rx_txt()

		# change the long string into a list of float for each channel
		buff_string = buff_string.strip('{}\n\r').replace("  ", "").split(',')
		buff = buff + list(map(float, buff_string))
		buff_string2 = buff_string2.strip('{}\n\r').replace("  ", "").split(',')
		buff2 = buff2 + list(map(float, buff_string2))

		# clean the graphs
		self.a1.clear()
		self.a2.clear()

		# update the graphs
		self.a1.plot(buff)
		self.a2.plot(buff2)
		canvas.draw()



def main():
	"""
	main function, init all the tkinter application	

	"""
	# create the root tkinter object
	root = Tk.Tk()
	# name the application's window
	root.wm_title("Nano")
	# create the tkinter application
	app=Application(master=root)
	# main loop of the application
	app.mainloop()  


if __name__ == '__main__':
	main()  
